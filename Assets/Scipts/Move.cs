﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour
{
    public LayerMask layer;     

    public float speed = 5.0f;
    private Rigidbody2D _rb;
    private Animator _anim;

    private void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        _anim = GetComponent<Animator>();
    }

    void Update()
    {
        // Collider2D[] collisions = Physics2D.OverlapCircleAll(transform.position, 3f, layer);

        float v = Input.GetAxisRaw("Vertical");
        float h = Input.GetAxisRaw("Horizontal");
        
        Vector2 dir = new Vector2(h, v);
        _rb.velocity = dir.normalized * speed;
        _anim.SetBool("flying", (v > 0));
    }

    /*private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere(transform.position, 3f);
    }*/
}
